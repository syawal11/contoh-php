<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Berlatih String</h2>
    <?php
        
        echo "<h3>Contoh 1 </h3>";
        $kalimat1 = "PHP is never old";
        echo "Kalimat pertama : ". $kalimat1 . "<br>";
        echo "Jumlah Karakter kalimat pertama : ".strlen($kalimat1). "<br>";
        echo "Jumlah Kata : ".str_word_count($kalimat1) ."<br><br>";

        echo "<h3>Contoh 2</h3>";
        $string2 = "I Love PHP";
        echo "Kalimat kedua : ". $string2."<br>";
        echo "Kata Pertama : ". substr($string2,0,1) ."<br>";    
        echo "Kata Kedua : ". substr($string2,2,4) ."<br>";
        echo "Kata Ketiga : ". substr($string2,7,3) ."<br>";

        echo "<h3>Contoh 3</h3>";
        $string3 = "PHP is old but good";
        echo "Kalimat ketiga : ". $string3."<br>";
        echo "Ganti Kalimat ketiga : ". str_replace("good","awsome",$string3);

    ?>
</body>
</html>