<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Berlatih Array</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ];
        print_r($kids);
        $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray" ];
        print_r($adults);



        echo "<h3>Soal 2</h3>";
        echo "Cast Stranger Things:";
        echo "<br>";
        echo "Total kids ". count($kids);
        echo "<ol>";
        echo "<li>" . $kids[0] ."</li>";
        echo "<li>" . $kids[1] ."</li>";
        echo "<li>" . $kids[2] ."</li>";
        echo "<li>" . $kids[3] ."</li>";
        echo "<li>" . $kids[4] ."</li>";
        echo "<li>" . $kids[5] ."</li>";
        echo "</ol>";

        echo "Total adults ". count($adults);
        echo "<ol>";
        echo "<li>" . $adults[0] ."</li>";
        echo "<li>" . $adults[1] ."</li>";
        echo "<li>" . $adults[2] ."</li>";
        echo "<li>" . $adults[3] ."</li>";
        echo "<li>" . $adults[4] ."</li>";
        echo "</ol>";

        echo "<h3>Soal 3</h3>";
        $biodata = [
            ["Name" => "Will Bayers","Ages"=> "12","Aliases" => "Will the Wise","Status"=>"Alive"],
            ["Name" => "Mike Wheeler","Ages"=> "12","Aliases" => "Dungeon Master","Status"=>"Alive"],
            ["Name" => "Jim Hooper","Ages"=> "43","Aliases" => "Chief Hooper","Status"=>"Deceased"],
            ["Name" => "Eleven","Ages"=> "12","Aliases" => "El","Status"=>"Alive"]
        ];

        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
    ?>
</body>
</html>